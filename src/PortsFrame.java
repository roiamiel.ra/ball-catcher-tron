import com.fazecast.jSerialComm.SerialPort;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class PortsFrame extends JFrame {

    private static final int BAUD_RATE = 10000;
    private static final int DATA_BITS = 8;
    private static final int STOP_BIT = 1;

    private ArrayList<SerialPort> portsList = new ArrayList<>();
    private DefaultComboBoxModel portsModel = new DefaultComboBoxModel<String>();

    private int selectedIndex = -1;

    private static OutputStream outputStream = null;
    private static InputStream inputStream = null;

    private JPanel jPanel = new JPanel(new FlowLayout());
    private JComboBox portsJComboBox = new JComboBox(portsModel);
    private JButton refreshJButton = new JButton("Refresh");
    private JButton contorlJButton = new JButton("Control");
    private JButton getVoltageJButton = new JButton("Get Voltage");
    private JButton disconnectJButton = new JButton("Disconnect");

    private OnCalibrationChanged onCalibrationChanged;
    private ColorDetector colorDetector;

    private ActionListener portsSelectedListener = e -> {
        selectedIndex = portsJComboBox.getSelectedIndex();

        if (selectedIndex == -1) {
            showSelectPortError();
        } else {
            openPort(selectedIndex);
        }

        requestFocus();
    };

    public PortsFrame(ColorDetector newColorDetector, OnCalibrationChanged newOnCalibrationChanged) {
        super("Ports");

        onCalibrationChanged = newOnCalibrationChanged;
        colorDetector = newColorDetector;

        init();
    }

    private void init() {
        updatePortsComboBox();

        jPanel.add(portsJComboBox);
        jPanel.add(refreshJButton);
        jPanel.add(contorlJButton);
        jPanel.add(disconnectJButton);
        jPanel.add(getVoltageJButton);
        contorlJButton.addActionListener(e -> requestFocus());
        refreshJButton.addActionListener(e -> updatePortsComboBox());
        disconnectJButton.addActionListener(e -> {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            selectedIndex = -1;
            portsJComboBox.setSelectedIndex(-1);
        });

        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_NUMPAD1:
                        System.out.println("1 pressed");
                        RobotController.setMotorsStatus(
                                outputStream,
                                new Vector(-1,
                                        1)
                        );
                        break;
                    case KeyEvent.VK_NUMPAD2:
                        System.out.println("2 pressed");
                        RobotController.setMotorsStatus(
                                outputStream,
                                new Vector(-1,
                                        0)
                        );
                        break;
                    case KeyEvent.VK_NUMPAD3:
                        System.out.println("3 pressed");
                        RobotController.setMotorsStatus(
                                outputStream,
                                new Vector(-1,
                                        -1)
                        );
                        break;
                    case KeyEvent.VK_NUMPAD4:
                        System.out.println("4 pressed");
                        RobotController.setMotorsStatus(
                                outputStream,
                                new Vector(0,
                                        1)
                        );
                        break;
                    case KeyEvent.VK_NUMPAD5:
                        System.out.println("5 pressed");
                        RobotController.setMotorsStatus(
                                outputStream,
                                new Vector(0,
                                        0)
                        );
                        break;
                    case KeyEvent.VK_NUMPAD6:
                        System.out.println("6 pressed");
                        RobotController.setMotorsStatus(
                                outputStream,
                                new Vector(0,
                                        -1)
                        );
                        break;
                    case KeyEvent.VK_NUMPAD7:
                        System.out.println("7 pressed");

                        RobotController.setMotorsStatus(
                                outputStream,
                                new Vector(1,
                                        1)
                        );
                        break;
                    case KeyEvent.VK_NUMPAD8:
                        System.out.println("8 pressed");

                        RobotController.setMotorsStatus(
                                outputStream,
                                new Vector(1,
                                        0)
                        );
                        break;
                    case KeyEvent.VK_NUMPAD9:
                        System.out.println("9 pressed");
                        RobotController.setMotorsStatus(
                                outputStream,
                                new Vector(1,
                                        -1)
                        );
                        break;
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                RobotController.setMotorsStatus(
                        outputStream,
                        new Vector(0,
                                0)
                );
            }
        });

        JButton calibrateButton = new JButton("Calibrate");
        jPanel.add(calibrateButton);
        calibrateButton.addActionListener(e -> onCalibrationChanged.onChecnged(Calibrate.calibrate(getOutputStream(), colorDetector)));

        add(jPanel);
        setSize(300, 300);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setFocusable(true);
    }

    private void updatePortsComboBox() {
        removeAllActionsListeners(portsJComboBox);

        portsList.clear();
        portsList.addAll(Arrays.asList(SerialPort.getCommPorts()));

        portsModel.removeAllElements();
        for (int i = 0; i < portsList.size(); i++) {
            portsModel.addElement(portsList.get(i).getDescriptivePortName());
        }

        portsJComboBox.setSelectedIndex(-1);
        selectedIndex = -1;

        portsJComboBox.addActionListener(portsSelectedListener);
    }

    private boolean openPort(int index) {
        SerialPort port = portsList.get(index);

        port.setBaudRate(BAUD_RATE);
        port.setNumDataBits(DATA_BITS);
        port.setNumStopBits(STOP_BIT);

        if(!port.openPort()) {
            updatePortsComboBox();
            showPortNotOpendError();

            return false;
        }

        if(outputStream != null) {
            try { outputStream.close(); } catch (IOException e) { e.printStackTrace(); }
        }

        if (inputStream != null) {
            try { inputStream.close(); } catch (IOException e) { e.printStackTrace(); }
        }

        outputStream = port.getOutputStream();
        inputStream = port.getInputStream();

        return true;
    }

    public OutputStream getOutputStream() {
        return outputStream;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    private void removeAllActionsListeners(JComboBox<String> jComboBox) {
        ActionListener[] listeners = jComboBox.getActionListeners();

        for (int i = 0; i < listeners.length; i++) {
            jComboBox.removeActionListener(listeners[i]);
        }
    }

    private void showSelectPortError() {
        Toast.showToast(jPanel, "select port first");
    }

    private void showPortNotOpendError() {
        Toast.showToast(jPanel, "can't open this port");
    }
}
