import java.awt.Color;
import java.io.InputStream;
import java.io.OutputStream;

public class Run {

    private static final int MAX_RANGE = 50;
    private static boolean running = false;

    private static boolean exit = false;

    private static int lastX = -1;
    private static int lastY = -1;
    private static long lastCheckedTime = -1;

    public static void moveTo (OutputStream outputStream, InputStream inputStream, ColorDetector colorDetector, VectorViewer vectorViewer, Vector[][] basisVectors, Vector point) {
        if ((Math.abs(colorDetector.getLastX() - point.get(0)) <= MAX_RANGE) && (Math.abs(colorDetector.getLastY() - point.get(1)) <= MAX_RANGE)) {
            RobotController.setMotorsStatus(outputStream, RobotController.SPEED_STOP);
            return;
        }

        running = true;
        exit = false;

        try {

            Vector newPoint = new Vector(2);

            do {
                newPoint.update((point.get(0)) - (colorDetector.getLastX()), (point.get(1)) - (colorDetector.getLastY()));
                Vector robotLocation = new Vector(colorDetector.getLastX(), colorDetector.getLastY());
                vectorViewer.updateVector("goToPoint", robotLocation, newPoint, Color.ORANGE);

                Vector projectionX = basisVectors[0][0].projectionBy(newPoint);
                Vector projectionY = basisVectors[1][0].projectionBy(newPoint);
                double normProjX = projectionX.vectorNormSqured();
                double normProjY = projectionY.vectorNormSqured();
                boolean XBiggerY = normProjX > normProjY;

                double ratio = (Math.min(normProjX, normProjY) / Math.max(normProjX, normProjY));

                // double speedX = (XBiggerY) ? RobotController.MAX_SPEED : (RobotController.MAX_SPEED) * ratio;
                // double speedY = (!XBiggerY) ? RobotController.MAX_SPEED : (RobotController.MAX_SPEED) * ratio;
                // double speedX = (XBiggerY) ? RobotController.MAX_SPEED : 0;
                // double speedY = (!XBiggerY) ? RobotController.MAX_SPEED : 0;

                double speedX = (XBiggerY) ? 1 : 0;
                double speedY = (!XBiggerY) ? 1 : 0;


                double drawX = (XBiggerY) ? 40 : (40) * ratio;
                double drawY = (!XBiggerY) ? 40 : (40) * ratio;

                if (newPoint.vectorDotProduct(basisVectors[0][0]) < 0) {
                    speedX *= -1;
                    drawX *= -1;
                }

                if (newPoint.vectorDotProduct(basisVectors[1][0]) < 0) {
                    speedY *= -1;
                    drawY *= -1;
                }

                vectorViewer.updateVector("speedVectorX", robotLocation, basisVectors[0][0].changeVectorSize(drawX).scalarMult(3), Color.magenta);
                vectorViewer.updateVector("speedVectorY", robotLocation, basisVectors[1][0].changeVectorSize(drawY).scalarMult(3), Color.magenta);

                Vector vectorSpeedX = RobotController.getSpeedAtAxis(basisVectors[0][1], (int) speedX);
                Vector vectorSpeedY = RobotController.getSpeedAtAxis(basisVectors[1][1], (int) speedY);

                // store robot location
                if (lastCheckedTime != -1 && lastCheckedTime - System.currentTimeMillis() >= 400) {
                    // reset calibrate
                    if (XBiggerY) {
                        basisVectors[0][0].update(colorDetector.getLastX() - lastX, colorDetector.getLastY() - lastY);
                    } else {
                        basisVectors[1][0].update(colorDetector.getLastX() - lastX, colorDetector.getLastY() - lastY);
                    }

                    lastCheckedTime = System.currentTimeMillis();
                }

                if (RobotController.setMotorsStatus(outputStream, Vector.vectorSum(vectorSpeedX, vectorSpeedY))) {
                    lastCheckedTime = System.currentTimeMillis();
                    lastX = colorDetector.getLastX();
                    lastY = colorDetector.getLastY();
                }

            } while (!exit && (Math.abs(colorDetector.getLastX() - point.get(0)) > MAX_RANGE) || (Math.abs(colorDetector.getLastY() - point.get(1)) > MAX_RANGE));

            RobotController.setMotorsStatus(outputStream, RobotController.SPEED_STOP);

            vectorViewer.removeComp("speedVectorX");
            vectorViewer.removeComp("speedVectorY");
            vectorViewer.removeComp("goToPoint");

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            running = false;
        }
    }

    public static void exit(OutputStream outputStream) {
        exit = true;

        if (outputStream != null) {
            RobotController.setMotorsStatus(outputStream, RobotController.SPEED_STOP);
        }
    }

    public static boolean isRunning() {
        return running;
    }
}