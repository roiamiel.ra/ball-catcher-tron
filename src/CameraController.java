import com.thomasdiewald.ps3eye.PS3Eye;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.ArrayList;

public class CameraController extends JFrame {

    private ArrayList<ColorDetectorInterface> colorDetectors;

    private static final PS3Eye.Resolution resolution = PS3Eye.Resolution.VGA;
    static final int CAMERA_WIDTH = resolution.w, CAMERA_HEIGHT = resolution.h;

    private static final int JUMP_VALUE = 4, SEGMENT_SIZE = 5;


    public int getHeight() {
        return CAMERA_HEIGHT;
    }

    public int getWidth() {
        return CAMERA_WIDTH;
    }

    private long lastFrameTime = -1;
    private long frameCounter = -1;

    private boolean draw = true;
    private int threshold = 60;

    private ImageViewerPanel imageViewerPanel;

    CameraController(ArrayList<ColorDetectorInterface> newColorDetectors) {
        colorDetectors = newColorDetectors;

        start(PS3Eye.getDevices()[0]);
    }

    private void start(PS3Eye webcam) {
        double[] hsv = new double[3];

        int[] onCounters = new int[colorDetectors.size()];

        // top, left, bottom, right
        int[][] points = new int[colorDetectors.size()][4];

        // install the camera
        webcam.init(60, resolution, PS3Eye.Format.BGR);
        webcam.start();

        // install camera viewer
        imageViewerPanel = new ImageViewerPanel(new Dimension(CAMERA_WIDTH, CAMERA_HEIGHT));
        imageViewerPanel.setLayout(new FlowLayout());
        imageViewerPanel.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        imageViewerPanel.setVisible(true);

        new Thread(() -> {
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

            int counter;

            boolean founded;

            BufferedImage bufferedImage = new BufferedImage(CAMERA_WIDTH, CAMERA_HEIGHT, BufferedImage.TYPE_3BYTE_BGR);
            byte[] image = ((DataBufferByte) bufferedImage.getRaster().getDataBuffer()).getData();

            while (webcam.isStreaming()) {
                webcam.getFrame(image);

                BufferedImage hsvMapImage = null;
                if (isDrawHSVMap()) {
                    hsvMapImage = new BufferedImage(CAMERA_WIDTH, CAMERA_HEIGHT, BufferedImage.TYPE_INT_RGB);
                }

                for (int i = 0; i < onCounters.length; i++) {
                    for (int j = 0; j < points[i].length; j++) {
                        points[i][j] = -1;
                    }
                }

                for (int w = 0; w < CAMERA_WIDTH; w += SEGMENT_SIZE) {
                    for (int h = 0; h < CAMERA_HEIGHT; h += SEGMENT_SIZE) {
                        counter = 0;

                        for (int i = 0; i < onCounters.length; i++) {
                            onCounters[i] = 0;
                        }

                        for (int segmentw = w; segmentw < w + SEGMENT_SIZE + 1 && segmentw < CAMERA_WIDTH; segmentw += getJumpValue()) {
                            for (int segmenth = h; segmenth < h + SEGMENT_SIZE + 1 && segmenth < CAMERA_HEIGHT; segmenth += getJumpValue()) {
                                int pos = (segmenth * 3 * CAMERA_WIDTH) + (segmentw * 3);

                                int red = ((int) image[pos++] & 0xff);
                                int green = ((int) image[pos++] & 0xff);
                                int blue = ((int) image[pos] & 0xff);

                                RGBtoHSV(red, green, blue, hsv);

                                if (isDrawHSVMap() && hsvMapImage != null) {
                                    hsvMapImage.setRGB(segmentw, segmenth, 0xFFFFFF);
                                }

                                founded = false;

                                for (int i = 0; i < onCounters.length; i++) {
                                     if (colorDetectors.get(i).isColorMatch(hsv[0], hsv[1], hsv[2])) {
                                         if (!founded && colorDetectors.get(i).getDrawHSVMap() && hsvMapImage != null) {
                                             hsvMapImage.setRGB(segmentw, segmenth, 0);
                                         }

                                         onCounters[i]++;
                                         founded = true;
                                     }
                                }

                                counter++;
                            }
                        }

                        double percent = 100.0 / counter;

                        for (int i = 0; i < onCounters.length; i++) {
                            if (onCounters[i] * percent >= threshold) {
                                if (h < points[i][0] || points[i][0] == -1) points[i][0] = h;
                                if (w < points[i][1] || points[i][1] == -1) points[i][1] = w;
                                if ((h + SEGMENT_SIZE) > points[i][2] || points[i][2] == -1) points[i][2] = (h + SEGMENT_SIZE);
                                if ((w + SEGMENT_SIZE) > points[i][3] || points[i][3] == -1) points[i][3] = (w + SEGMENT_SIZE);
                            }
                        }
                    }
                }

                for (int i = 0; i < onCounters.length; i++) {
                    int sizeH = points[i][2] - points[i][0], sizeW = points[i][3] - points[i][1];

                    if (draw) {
                        Graphics2D graph = bufferedImage.createGraphics();
                        graph.setStroke(new BasicStroke(3));

                        graph.setColor(Color.RED);
                        graph.setColor(Color.MAGENTA);
                        graph.drawRect(points[i][1], points[i][0], sizeW, sizeH);

                        graph.setColor(Color.BLUE);
                        Shape theCircle = new Ellipse2D.Double(points[i][1] + sizeW / 2.0 - 3, points[i][0] + sizeH / 2.0 - 3, 6, 6);
                        graph.fill(theCircle);
                    }

                    int lastX = points[i][1] + (sizeW / 2);
                    int lastY = points[i][0] + (sizeH / 2);

                    colorDetectors.get(i).lastLocationChanged(lastX == -1 ? -1 : getWidth() - lastX, lastY == -1 ? -1 : getHeight() - lastY);
                }

                frameCounter++;

                if (lastFrameTime == -1) {
                    lastFrameTime = System.currentTimeMillis();
                    frameCounter = 0;
                }

                double framesTime = System.currentTimeMillis() - lastFrameTime;
                if (framesTime >= 2000) {
                    System.out.println("fps: " + (int) ((frameCounter / framesTime) * 1000));

                    lastFrameTime = -1;
                }

                if (draw) imageViewerPanel.setImage(bufferedImage, hsvMapImage);
            }
        }).start();
    }

    public ImageViewerPanel getImageViewPanel() {
        return imageViewerPanel;
    }

    private int getJumpValue() {
        return isDrawHSVMap() ? 1 : JUMP_VALUE;
    }

    private boolean isDrawHSVMap() {
        for (ColorDetectorInterface colorDetector : colorDetectors) {
            if (colorDetector.getDrawHSVMap()) return true;
        }

        return false;
    }

    private void RGBtoHSV(int red, int green, int blue, double[] arr) {
        if (arr.length != 3) throw new IllegalArgumentException();

        double r = red / 255.0;
        double g = green / 255.0;
        double b = blue / 255.0;

        double computedH;
        double computedS;
        double computedV = 0;

        double minRGB = Math.min(r, Math.min(g, b));
        double maxRGB = Math.max(r, Math.max(g, b));

        // Black-gray-white
        if (minRGB == maxRGB) {
            arr[0] = 0.0;
            arr[1] = 0.0;
            arr[2] = computedV * 100;

            return;
        }

        // Colors other than black-gray-white:
        double d = (r == minRGB) ? g - b : ((b == minRGB) ? r - g : b - r);
        double h = (r == minRGB) ? 3 : ((b == minRGB) ? 1 : 5);
        computedH = 60 * (h - d / (maxRGB - minRGB));
        computedS = (maxRGB - minRGB) / maxRGB;
        computedV = maxRGB;

        arr[0] = computedH;
        arr[1] = computedS * 100;
        arr[2] = computedV * 100;
    }

    public class ImageViewerPanel extends JFrame {

        private Dimension dimension;
        private BufferedImage image1 = null;
        private ArrayList<BufferedImage> imagesList = new ArrayList<>();

        ImageViewerPanel(Dimension Dimension) {
            dimension = Dimension;
            setSize(dimension);
            printComponents(getGraphics());

        }

        void setImage(BufferedImage Image1, BufferedImage... image) {
            image1 = Image1;

            imagesList.clear();
            if (image != null) {
                for (BufferedImage bufferedImage : image) {
                    if (bufferedImage != null) {
                        imagesList.add(bufferedImage);
                    }
                }
            }

            printComponents(getGraphics());
        }

        @Override
        public void printComponents(Graphics g) {
            if (g == null || image1 == null) return;

            int h = 0;

            Graphics2D graphics2D = (Graphics2D) g;
            graphics2D.drawImage(image1, image1.getWidth(), h, -image1.getWidth(), image1.getHeight(), (img, infoflags, x, y, width, height) -> false);
            h += image1.getHeight();

            for (BufferedImage bufferedImage : imagesList) {
                graphics2D.drawImage(bufferedImage, bufferedImage.getWidth(), h, -bufferedImage.getWidth(), bufferedImage.getHeight(), (img, infoflags, x, y, width, height) -> false);
                h += bufferedImage.getHeight();
            }

            setSize(getWidth(), h);
        }
    }


}
