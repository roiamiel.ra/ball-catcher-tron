public class Vector {

    private int size;
    private double[] vector;

    public Vector(double... vectorArray) {
        size = vectorArray.length;
        vector = new double[size];

        System.arraycopy(vectorArray, 0, vector, 0, vectorArray.length);
    }

    public Vector(int newSize) {
        size = newSize;
        vector = new double[size];
    }

    public Vector(Vector oldVector) {
        size = oldVector.getSize();
        vector = new double[size];

        for (int i = 0; i < size; i++) {
            vector[i] = oldVector.get(i);
        }
    }

    public int getSize() {
        return size;
    }

    public double get(int index) {
        return vector[index];
    }

    public void set(int index, double value) {
        vector[index] = value;
    }

    public void update(double... newValues) {
        if (newValues.length != size) {
            throw new IllegalArgumentException("Wrong size");
        }

        System.arraycopy(newValues, 0, vector, 0, newValues.length);
    }

    public void update(Vector vec) {
        if (vec.size != size) {
            throw new IllegalArgumentException("Wrong size");
        }

        System.arraycopy(vec.vector, 0, vector, 0, vec.size);
    }


    public double vectorDotProduct(Vector vec) {
        double sum = 0;

        if(vec.size != size){
            throw new IllegalArgumentException("vectors cant be multiplied(different size)");
        }

        for (int i = 0; i < size; i++) {
            sum += vec.vector[i] * vector[i];
        }

        return sum;
    }

    public static Vector vectorSum(Vector vector1, Vector vector2) {
        if(vector1.size != vector2.size){
            throw new IllegalArgumentException("vectors cant be added(different size)");
        }

        Vector sum = new Vector(vector1.size);

        for (int i = 0; i < vector1.size; i++) {
            sum.set(i, vector1.get(i) + vector2.get(i));
        }

        return sum;
    }

    public double vectorNormSqured() {
        return vectorDotProduct(this);
    }

    public double vectorNorm() {
        return Math.sqrt(vectorNormSqured());
    }

    public Vector scalarMult(double scalar) {
        Vector vec = new Vector(this);

        for (int i = 0; i < vec.size; i++) {
            vec.vector[i] *= scalar;
        }

        return vec;
    }

    public Vector changeVectorSize(double size) {
        Vector newVector = new Vector(this);

        return newVector.scalarMult(size / vectorNorm());
    }

    public Vector projectionBy(Vector by) {
        return scalarMult(vectorDotProduct(by)).scalarMult(1.0 / vectorNormSqured());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Vector) {
            if (((Vector) obj).size == size) {
                for (int i = 0; i < size; i++) {
                    if (((Vector) obj).get(i) != get(i)) {
                        return false;
                    }
                }

                return true;
            } else {
                return false;
            }
        }

        return super.equals(obj);
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder("(");

        for (int i = 0; i < vector.length; i++) {
            string.append(vector[i]);

            if (i != vector.length - 1) {
                string.append(", ");
            }
        }

        string.append(")");

        return string.toString();
    }
}
