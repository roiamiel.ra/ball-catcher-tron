
import java.io.OutputStream;

public class Calibrate {
    private static final int measureTime = 350;
    public static Vector[][] calibrate(OutputStream outputStream, ColorDetector colorDetector) {

        Vector basisVectorX[] = new Vector[2];
        Vector basisVectorY[] = new Vector[2];

        Vector startPoint = new Vector(colorDetector.getLastX(), colorDetector.getLastY());

        RobotController.setMotorsStatus(outputStream , RobotController.getSpeedAtAxis(RobotController.X_AXIS, 1));
        Time.delay(measureTime);
        RobotController.setMotorsStatus(outputStream , RobotController.SPEED_STOP);

        Time.delay(400);

        Vector newPoint = new Vector(colorDetector.getLastX() , colorDetector.getLastY());
        basisVectorX[0] = new Vector(newPoint.get(0) - startPoint.get(0), newPoint.get(1) - startPoint.get(1));
        basisVectorX[1] = RobotController.X_AXIS;

        RobotController.setMotorsStatus(outputStream , RobotController.getSpeedAtAxis(RobotController.Y_AXIS, 1));
        Time.delay(measureTime);
        RobotController.setMotorsStatus(outputStream , RobotController.SPEED_STOP);

        Time.delay(400);

        newPoint = new Vector(colorDetector.getLastX() , colorDetector.getLastY());
        basisVectorY[0] = new Vector(newPoint.get(0) - startPoint.get(0) - basisVectorX[0].get(0),
                                          newPoint.get(1) - startPoint.get(1) - basisVectorX[0].get(1));
        basisVectorY[1] = RobotController.Y_AXIS;

        return new Vector[][]{basisVectorX , basisVectorY};
    }
}
