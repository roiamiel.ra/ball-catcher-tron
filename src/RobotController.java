import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;
import java.util.UnknownFormatConversionException;

public class RobotController {

    private static final int NUMBER_OF_MOTORS = 2;

    static final Vector X_AXIS = new Vector(0, 1);
    static final Vector Y_AXIS = new Vector(1, 0);
    static final Vector SPEED_STOP = new Vector(0, 0);

    private static Vector lastSentVector = null;

    private static final byte VALIDATION_BITS = (byte) 0b01010000;

    public static Vector getSpeedAtAxis(Vector axis, int speed) {
        return axis.scalarMult(speed);
    }

    public static boolean setMotorsStatus(OutputStream portOutputStream, Vector speedsVector) {
        if (lastSentVector != null && lastSentVector.equals(speedsVector)) {
            return false;
        } else {
            lastSentVector = new Vector(speedsVector);
        }

        // check if speeds not missing
        if (speedsVector.getSize() != NUMBER_OF_MOTORS) {
            throw new IllegalArgumentException("The size of the speedsVector must be the number of motors");
        }

        byte message = (byte) (VALIDATION_BITS + (byte) ((speedsVector.get(0) != 0 ? 0b00000001 : 0) + (speedsVector.get(0) > 0 ? 0b00000010 : 0) +
                                (speedsVector.get(1) != 0 ? 0b00000100 : 0) + (speedsVector.get(1) > 0 ? 0b000001000 : 0)));

        // send the buffer to the robot via port
        try {
            System.out.println(message);
            portOutputStream.write(message);
        } catch (IOException e) {
            System.out.println("error: " + e.toString());
            return false;
        }

        return true;
    }

}
