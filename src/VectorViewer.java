import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

public class VectorViewer extends JFrame {

    private BufferedImage bufferedImage;
    private Graphics2D graphics2D;
    private HashMap<String, ArrayList<Vector>> vectors = new HashMap<>();
    private HashMap<String, Color> colors = new HashMap<>();

    private int width = 0;
    private int height = 0;

    public VectorViewer(int newWidth, int newHeight) {
        if (Main.fastMode) return;

        width = newWidth;
        height = newHeight;

        bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        graphics2D = bufferedImage.createGraphics();
        setSize(width, height);
        getContentPane().setLayout(new FlowLayout());
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);

        try { printComponents(graphics2D); } catch (Exception e) { }
    }

    public void updateVector(String name, Vector midPoint, Vector vector, Color color) {
        if (Main.fastMode) return;

        vectors.computeIfAbsent(name, k -> new ArrayList<>());
        ArrayList<Vector> comp = vectors.get(name);
        comp.clear();
        comp.add(midPoint);
        comp.add(Vector.vectorSum(midPoint, vector));

        colors.put(name, color);

        try { printComponents(graphics2D); } catch (Exception e) { }
    }

    public void removeComp(String name) {
        if (Main.fastMode) return;

        vectors.remove(name);
    }

    public void updatePoint(String name, Vector vector, Color color) {
        if (Main.fastMode) return;

        vectors.computeIfAbsent(name, k -> new ArrayList<>());
        ArrayList<Vector> comp = vectors.get(name);
        comp.clear();
        comp.add(vector);

        colors.put(name, color);

        try { printComponents(graphics2D); } catch (Exception e) { }
    }

    public void printComponents(Graphics g) {
        if (Main.fastMode) return;

        // drew background
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, width, height);

        for (String name : vectors.keySet()) {
            ArrayList<Vector> points = vectors.get(name);

            switch (points.size()) {
                case 1:
                    // this is point
                    Vector point = points.get(0);

                    g.setColor(colors.get(name));

                    ((Graphics2D) g).fill(new Ellipse2D.Double((int) point.get(0), height - (int) point.get(1), 10, 10));

                    break;
                case 2:
                    // this is vector
                    g.setColor(colors.get(name));

                    Vector startPoint = points.get(0);
                    Vector endPoint = points.get(1);

                    drawArrowLine(g, (int) startPoint.get(0), (int) startPoint.get(1), (int) endPoint.get(0), (int) endPoint.get(1), 5, 5);

                    break;
            }
        }

        getGraphics().drawImage(bufferedImage, 0, 0, this);
    }

    /**
     * Draw an arrow line between two points.
     * @param g the graphics component.
     * @param x1 x-position of first point.
     * @param y1 y-position of first point.
     * @param x2 x-position of second point.
     * @param y2 y-position of second point.
     * @param d  the width of the arrow.
     * @param h  the height of the arrow.
     */
    private void drawArrowLine(Graphics g, int x1, int y1, int x2, int y2, int d, int h) {
        if (Main.fastMode) return;

        int dx = x2 - x1, dy = y2 - y1;
        double D = Math.sqrt(dx*dx + dy*dy);
        double xm = D - d, xn = xm, ym = h, yn = -h, x;
        double sin = dy / D, cos = dx / D;

        x = xm*cos - ym*sin + x1;
        ym = xm*sin + ym*cos + y1;
        xm = x;

        x = xn*cos - yn*sin + x1;
        yn = xn*sin + yn*cos + y1;
        xn = x;

        int[] xpoints = {x2, (int) xm, (int) xn};
        int[] ypoints = {height - y2, height - (int) ym, height - (int) yn};

        g.drawLine(x1, height - y1, x2, height - y2);
        g.fillPolygon(xpoints, ypoints, 3);
    }

}
