public interface ColorDetectorInterface {

    ColorPackage getColorPackage();

    boolean getDrawHSVMap();

    void lastLocationChanged(int newX, int newY);

    boolean isColorMatch(double h, double s, double v);

}
