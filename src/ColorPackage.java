public class ColorPackage {

    private int[] data = {0, 0, 0, 0, 0, 0};

    public ColorPackage(String shortName) {
        int i = 0;
        for (String s : shortName.split(",")) {
            data[i] = Integer.parseInt(s);
            i++;
        }
    }

    public int get(int index) {
        return data[index];
    }

    public void set(int index, int value) {
        data[index] = value;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            stringBuilder.append(data[i]);
            if (i != data.length -1) {
                stringBuilder.append(",");
            }
        }

        return stringBuilder.toString();
    }

    public static ColorPackage getDefualtColorPackage() {
        return new ColorPackage("0,100,0,100,0,100");
    }
}
