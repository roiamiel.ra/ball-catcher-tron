import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.WindowConstants;

public class ColorDetector extends JFrame implements ColorDetectorInterface {

    private ColorPackage colorPackage;

    private boolean drawSegmentMap = false;
    private boolean drawHSVMap = false;

    private int lastX = -1;
    private int lastY = -1;

    public ColorDetector(String name, ColorPackage newColorPackage) {
        super(name);

        colorPackage = newColorPackage;

        init();
    }

    private void init() {
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        JSlider startHSlider = new JSlider(0, 360, colorPackage.get(0));
        JSlider startSSlider = new JSlider(0, 100, colorPackage.get(1));
        JSlider startVSlider = new JSlider(0, 100, colorPackage.get(2));
        JSlider endHSlider = new JSlider(0, 360, colorPackage.get(3));
        JSlider endSSlider = new JSlider(0, 100, colorPackage.get(4));
        JSlider endVSlider = new JSlider(0, 100, colorPackage.get(5));

        JButton printStateButton = new JButton("Print State");
        JCheckBox drawHSVMapCheckBox = new JCheckBox("Draw HSV Map");

        startHSlider.addChangeListener(e -> colorPackage.set(0, startHSlider.getValue()));
        startSSlider.addChangeListener(e -> colorPackage.set(1, startSSlider.getValue()));
        startVSlider.addChangeListener(e -> colorPackage.set(2, startVSlider.getValue()));
        endHSlider.addChangeListener(e -> colorPackage.set(3, endHSlider.getValue()));
        endSSlider.addChangeListener(e -> colorPackage.set(4, endSSlider.getValue()));
        endVSlider.addChangeListener(e -> colorPackage.set(5, endVSlider.getValue()));

        drawHSVMapCheckBox.addChangeListener(e -> drawHSVMap = drawHSVMapCheckBox.isSelected());

        printStateButton.addActionListener(e -> System.out.println(colorPackage.toString()));

        add(new JLabel("Hue Start"));
        add(startHSlider);
        add(new JLabel("Hue End"));
        add(endHSlider);
        add(new JLabel("Saturation Start"));
        add(startSSlider);
        add(new JLabel("Saturation End"));
        add(endSSlider);
        add(new JLabel("Value Start"));
        add(startVSlider);
        add(new JLabel("Value End"));
        add(endVSlider);
        add(new JLabel());
        add(drawHSVMapCheckBox);
        add(printStateButton);

        setSize(new Dimension(400, 600));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
        requestFocusInWindow();
    }

    @Override
    public ColorPackage getColorPackage() {
        return colorPackage;
    }

    @Override
    public boolean getDrawHSVMap() {
        return drawHSVMap;
    }

    @Override
    public void lastLocationChanged(int newX, int newY) {
        lastX = newX;
        lastY = newY;
    }

    @Override
    public boolean isColorMatch(double h, double s, double v) {
        return (h >= getColorPackage().get(0) && h <= getColorPackage().get(3) &&
                s >= getColorPackage().get(1) && s <= getColorPackage().get(4) &&
                v >= getColorPackage().get(2) && v <= getColorPackage().get(5));

    }

    public int getLastX() {
        return lastX;
    }

    public int getLastY() {
        return lastY;
    }
}
