import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

public class Main {

    private static Vector[][] calibration;

    private static CameraController cameraController;

    private static Vector goToVector = new Vector(2);

    private static PortsFrame portsFrame;

    private static boolean useScreenClicked = false;

    public static boolean fastMode = true;

    public static void main(String[] args) {
        VectorViewer vectorViewer = new VectorViewer(CameraController.CAMERA_WIDTH, CameraController.CAMERA_HEIGHT);

        ColorDetector robotDetector = new ColorDetector("Robot Detector", new ColorPackage("13,0,46,173,25,57")) {
            @Override
            public void lastLocationChanged(int newX, int newY) {
                super.lastLocationChanged(newX, newY);

                if (calibration == null) return;

                Vector robotLocation = new Vector(newX, newY);
                vectorViewer.updatePoint("robotLocation", robotLocation, Color.CYAN);

                if (calibration != null) {
                    vectorViewer.updateVector("calibrationX", robotLocation, new Vector(calibration[0][0]).changeVectorSize(100), Color.BLUE);
                    vectorViewer.updateVector("calibrationY", robotLocation, new Vector(calibration[1][0]).changeVectorSize(100), Color.BLUE);
                }
            }
        };

        ColorDetector ballDetector = new ColorDetector("Ball Detector", new ColorPackage("195,43,58,225,81,91")) {
            @Override
            public void lastLocationChanged(int newX, int newY) {
                super.lastLocationChanged(newX, newY);

                if (useScreenClicked) return;

                if (robotDetector.getLastX() <= 30 || CameraController.CAMERA_WIDTH - 30 <= robotDetector.getLastX() || robotDetector.getLastY() <= 30 || CameraController.CAMERA_HEIGHT - 30 <= robotDetector.getLastY()) {
                    if (portsFrame != null) Run.exit(portsFrame.getOutputStream());
                    return;
                }

                if (newX == -1 || newY == -1) {
                    if (Run.isRunning()) Run.exit(portsFrame.getOutputStream());
                    return;
                }

                goToVector.update(newX, newY);

                if (!Run.isRunning()) {
                    new Thread(() -> Run.moveTo(portsFrame.getOutputStream(), portsFrame.getInputStream(), robotDetector, vectorViewer, calibration, goToVector)).start();
                }
            }
        };

        ArrayList<ColorDetectorInterface> detectors = new ArrayList<>();
        detectors.add(robotDetector);
        detectors.add(ballDetector);

        try { cameraController = new CameraController(detectors); } catch (Exception ignored) {}
        portsFrame = new PortsFrame(robotDetector, newCalibration -> calibration = newCalibration);

        cameraController.getImageViewPanel().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (!useScreenClicked) return;

                if (robotDetector.getLastX() <= 30 || CameraController.CAMERA_WIDTH - 30 <= robotDetector.getLastX() || robotDetector.getLastY() <= 30 || CameraController.CAMERA_HEIGHT - 30 <= robotDetector.getLastY()) {
                    if (portsFrame != null) Run.exit(portsFrame.getOutputStream());
                    return;
                }

                Vector pointLocation = new Vector(e.getX(), CameraController.CAMERA_HEIGHT - e.getY());

                if (pointLocation.get(0) == -1 || pointLocation.get(1) == -1) {
                    if (Run.isRunning()) Run.exit(portsFrame.getOutputStream());
                    return;
                }

                if (!Run.isRunning()) {
                    new Thread(() -> Run.moveTo(portsFrame.getOutputStream(), portsFrame.getInputStream(), robotDetector, vectorViewer, calibration, pointLocation)).start();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

}
