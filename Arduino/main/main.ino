#include <SoftwareSerial.h>

// Comunication speed (bluethooth and usb port)
#define BAUD_RATE 10000

#define VALIDATION_BITS 0b01010000
#define VALIDATION_BITS_MASK 0b01110000

// Motors constants
#define NUMBER_OF_MOTORS 2
#define NUMBER_OF_MOTOR_PINS 2

// Motors legs map
// <Enable, Plus, Minus>
#define MOTOR_0_LEGS {9, 10}
#define MOTOR_1_LEGS {11, 12}

short motorsLegs[NUMBER_OF_MOTORS][NUMBER_OF_MOTOR_PINS] = {MOTOR_0_LEGS, MOTOR_1_LEGS};

void setMotorSpeed(int index, bool on, bool forword);

// bluetoothe serial
SoftwareSerial BTSerial(7, 8);      // (TXD, RXD) of HC-05

void setup() {
  // set serial speed
  Serial.begin(BAUD_RATE);
  BTSerial.begin(BAUD_RATE);

  // init motors legs as output
  for (int i = 0; i < NUMBER_OF_MOTORS; i++) {
    for (int j = 0; j < NUMBER_OF_MOTOR_PINS; j++) {
      pinMode(motorsLegs[i][j], OUTPUT);
    }

    // stop the motor without immediately breaking
    setMotorSpeed(i, false, false);
  }
}

void loop() {
  if (BTSerial.available() > 0) {
    // get the data form the serial port
    byte data = BTSerial.read();

    // check if the data contains the vaidation bits
    if ((VALIDATION_BITS_MASK & data) == VALIDATION_BITS) {
      setMotorSpeed(0, (data & 0b00000001) != 0, (data & 0b00000010) != 0);
      setMotorSpeed(1, (data & 0b00000100) != 0, (data & 0b00001000) != 0);
    }
  }
}

// This function change the speed of the motor in the I\O level
void setMotorSpeed(int index, bool on, bool forword) {
    // get this spesific motor pines by the pines map
    int pinA = motorsLegs[index][0];
    int pinB = motorsLegs[index][1];

    // change the motor speed in the I\O level
    digitalWrite(pinA, on && forword ? HIGH : LOW);
    digitalWrite(pinB, on && !forword ? HIGH : LOW);
}